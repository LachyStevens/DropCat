import java.io.*;
import java.net.InetAddress;
import java.net.Socket;

public class Client {

    Socket          SOCKET;

    public Client(){
        try {
            Settings.ADDRESS = InetAddress.getLocalHost().getHostAddress();



            Socket client = new Socket(Settings.ADDRESS, Settings.PORT);

            System.out.println("Just connected to " + client.getRemoteSocketAddress());
            OutputStream outToServer = client.getOutputStream();
            DataOutputStream out = new DataOutputStream(outToServer);

            out.writeUTF("Hello from " + client.getLocalSocketAddress());
            InputStream inFromServer = client.getInputStream();
            DataInputStream in = new DataInputStream(inFromServer);

            System.out.println("Server says " + in.readUTF());
            client.close();
        }catch(IOException e) {
            e.printStackTrace();
        }
    }
}