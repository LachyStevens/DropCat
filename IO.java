import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

public class IO {

    public static void WriteFile(Path path, String name, byte[] content) throws IOException {
        Files.write(Paths.get(path.toString(),name), content, StandardOpenOption.CREATE, StandardOpenOption.CREATE_NEW);
    }

    public static byte[] ReadFile(Path path) throws IOException {
        return Files.readAllBytes(path);
    }

}
