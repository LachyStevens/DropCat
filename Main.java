import java.io.IOException;
import java.net.InetAddress;

public class Main {

    public static void main(String[] args) {



    }

    public static void searchHosts(String subnet) {
        try {
            int timeout=500;
            for (int i=1;i<255;i++){
                String host=subnet + "." + i;
                if (InetAddress.getByName(host).isReachable(timeout)){
                    Settings.KNOWN_HOSTS.add(host);
                }
            }
        } catch (IOException e) {e.printStackTrace();}
    }

    public static boolean addHost(String address){
        try {
            if (InetAddress.getByName(address).isReachable(1000)) {
                Settings.KNOWN_HOSTS.add(address);
                return true;
            }
            return false;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

}
