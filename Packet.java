public class Packet {

    private String  name;
    private byte[]  content;

    public Packet(String packet) {
        String[] split = packet.split(" ");
        name = split[0];
        content = split[1].getBytes();
    }

    public Packet(String name, byte[] content) {
        this.name = name;
        this.content = content;
    }

    public byte[] getContent() {
        return content;
    }

    public String getName() {
        return name;
    }
}
