import java.io.DataInputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Observable;

public class PortListener extends Observable implements Runnable {

    private ServerSocket serverSocket;
    private Socket       server;

    private Packet       mostRecentPacket;

    public boolean      listen,
                        waitingForResponse,
                        confirmedConnection;

    public PortListener(){
        listen = true;
        waitingForResponse = true;

        try {
            serverSocket = new ServerSocket(Settings.PORT);
            serverSocket.setSoTimeout(0);
        } catch (IOException e) {e.printStackTrace();}
    }

    @Override
    public void run() {
        try {
            while(listen){

                server = serverSocket.accept();
                // create some sort of prompt to accept file

                while (waitingForResponse) {
                    try {
                        Thread.sleep(100);
                    } catch (Exception e) {e.printStackTrace();}
                }
                // reset flag
                waitingForResponse = true;

                if (confirmedConnection) {
                    DataInputStream in = new DataInputStream(server.getInputStream());
                    mostRecentPacket = new Packet(in.readUTF());
                    setChanged();
                    notifyObservers();
                }
            }
        } catch (Exception e) {e.printStackTrace();}
    }

    public Packet getMostRecentPacket(){
        return mostRecentPacket;
    }

}
