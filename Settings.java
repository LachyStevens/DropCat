import java.nio.file.Path;
import java.util.ArrayList;

class Settings {

    static String            SUBNET;
    static int               PORT;

    static String            ADDRESS;

    static Path              DIRECTORY;

    static ArrayList<String> KNOWN_HOSTS;


    static {
        SUBNET  = "192.168.1";
        PORT    = 6066;

        KNOWN_HOSTS = new ArrayList<>();
    }

}
